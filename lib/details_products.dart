import 'package:crudmysql_api/plugin.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'product_model.dart';
import 'Edit_product.dart';

class Details extends StatefulWidget {
  final products product;

  Details({required this.product});

  @override
  _DetailsState createState() => _DetailsState();
}

class _DetailsState extends State<Details> {
  void deleteProduct(context) async {
    await http.post(
      Uri.parse("http://10.0.2.2/android/delete_product.php"),
      body: {
        'id': widget.product.pid.toString(),
      },
    );
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
  }

  void confirmDelete(context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: Text('Are you sure you want to delete this?'),
          actions: <Widget>[
            ElevatedButton.icon(
              label: Text('Yes'),
              icon: Icon(Icons.check_circle),
              style: ElevatedButton.styleFrom(
                primary: Colors.purple,
                onPrimary: Colors.white,
              ),
              onPressed: () => deleteProduct(context),
            ),
            ElevatedButton.icon(
              label: Text('No'),
              icon: Icon(Icons.cancel),
              style: ElevatedButton.styleFrom(
                primary: Colors.purple,
                onPrimary: Colors.white,
              ),
              onPressed: () => Navigator.of(context).pop(),
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavigationDrawer(),
      appBar: AppBar(
        backgroundColor: Colors.purple,
        centerTitle: true,
        title: Text('Product Details'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.delete),
            onPressed: () => confirmDelete(context),
          ),
        ],
      ),
        body: Padding(
        padding: const EdgeInsets.only(top: 10),
        child: Container(
        padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 10.0),
        height: MediaQuery.of(context).size.height * 0.35,
        width: MediaQuery.of(context).size.width * 1,
        child: Card(
        color: Colors.grey[300],
        shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(30.0),
        ),
        elevation: 15,
          child: Padding(
            padding: const EdgeInsets.all(40.0),
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget> [
                  Text(
                     "Product Name : ${widget.product.name}",
                      style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),
                  ),
                  SizedBox(height: 10,),
                  Text(
                      "Price : ${widget.product.price}",
                      style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),
                      ),
                  SizedBox(height: 10,),
                  Text(
                      "Description : ${widget.product.description}",
                      style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),
                      ),
                ],
              ),
            ),
          ),
    ),
        ),
        ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.edit),
        backgroundColor: Colors.purple,
        onPressed: () => Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => EditProduct(product: widget.product),
          ),
        ),
      ),
    );

  }
}
