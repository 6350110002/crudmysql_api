
import 'package:crudmysql_api/about.dart';
import 'package:crudmysql_api/add_product.dart';
import 'package:crudmysql_api/main.dart';
import 'package:flutter/material.dart';

class Plugin extends StatelessWidget {
  const Plugin({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
    );
  }
}
class NavigationDrawer extends StatelessWidget {
  const NavigationDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Drawer(
    child: SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          buildHeader(context),
          buildMenuItems(context),
        ],
      ),
    ),
  );
}

Widget buildHeader(BuildContext context) => Container(
  color: Colors.purple,
  child: Padding(
    padding: const EdgeInsets.all(20.0),
    child: Column(
      children: [
        Text('Product', style:
        TextStyle(
          fontSize: 30,color: Colors.white,fontWeight: FontWeight.w400,
        ),
        )
      ],
    ),
  ),
  padding: const EdgeInsets.all(24),
);
Widget buildMenuItems(BuildContext context) => Wrap(
  runSpacing: 10,
  children: [
    ListTile(
      leading: const Icon(Icons.home),
      title: const Text('Home'),
      onTap: (){
        Navigator.of(context).push(MaterialPageRoute(builder: (context)=>
            Home()));
      },
    ),
    ListTile(
      leading: const Icon(Icons.add_box),
      title: const Text('Add Product'),
      onTap: (){
        Navigator.of(context).push(MaterialPageRoute(builder: (context)=>
           AddProduct()));

      },
    ),
    ListTile(
      leading: const Icon(Icons.people_alt),
      title: const Text('About'),
      onTap: (){
        Navigator.of(context).push(MaterialPageRoute(builder: (context)=>
            About ()));

      },
    ),
  ],
);


