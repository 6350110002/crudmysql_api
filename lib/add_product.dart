import 'package:crudmysql_api/plugin.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class AddProduct extends StatefulWidget {
  @override
  _AddProductState createState() => _AddProductState();
}

class _AddProductState extends State<AddProduct> {
  // Handles text
  TextEditingController nameController = TextEditingController();
  TextEditingController priceController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();



  // Http post request to create new data
  Future _createproducts() async {
    return await http.post(
      Uri.parse("http://10.0.2.2/android/insert_product.php"),
      body: {

        "name": nameController.text,
        "price": priceController.text,
        "description": descriptionController.text
      },
    );
  }

  void _onConfirm(context) async {
    await _createproducts();

    // Remove all existing routes until the Home.dart, then rebuild Home.
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
  }
 final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavigationDrawer(),
      appBar: AppBar(
        backgroundColor: Colors.purple,
        centerTitle: true,
        title: Text("Add Product"),
      ),
      bottomNavigationBar: BottomAppBar(
        child: ElevatedButton.icon(
          label: Text('Save'),
          icon: Icon(Icons.save),
          style: ElevatedButton.styleFrom(
            primary: Colors.purple,
            onPrimary: Colors.white,
          ),
          onPressed: () {
            if(_formKey.currentState!.validate()){
              _formKey.currentState!.save();
                _onConfirm(context);
              }
          },
        ),
      ),
      body: Container(
        height: double.infinity,
        padding: EdgeInsets.all(20),
        child: Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
            Container(
                child: TextFormField(
                  controller: nameController,
                  decoration: InputDecoration(
                    labelText : 'Product Name',
                    prefixIcon: Icon(Icons.verified_user_rounded),
                    border: OutlineInputBorder()
                  ),
                  validator: (value) {

                    if (value!.isEmpty) {
                      return 'Please enter product name';

                    } else {
                      return null;
                    }
                  },
                )
            ),
              SizedBox(height: 10,),
              Container(
                  child: TextFormField(
                    controller: priceController,
                    decoration: InputDecoration(
                        labelText : 'Price',
                        prefixIcon: Icon(Icons.monetization_on),
                        border: OutlineInputBorder()
                    ),
                    keyboardType: TextInputType.number,
                    validator: (value) {

                      if (value!.isEmpty ||!RegExp('[0-9]').hasMatch(value)) {
                        return 'Please enter product price';

                      } else {
                        return null;
                      }
                    },
                  )
              ),
              SizedBox(height: 10,),
              Container(
                  child: TextFormField(
                    controller: descriptionController,
                    decoration: InputDecoration(
                        labelText : 'Description',
                        prefixIcon: Icon(Icons.library_books_rounded),
                        border: OutlineInputBorder()
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Please enter product description';
                      } else {
                        return null;
                      }
                    },
                  )
              ),
          ],
          ),

        ),
      ),
    );
  }
}
