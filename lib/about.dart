import 'package:crudmysql_api/plugin.dart';
import 'package:flutter/material.dart';

class About extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: NavigationDrawer(),
        appBar: AppBar(
          centerTitle: true,
          title: Text('About'),
          backgroundColor: Colors.purple,
        ),
        body: Container(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Container(
                      width: 400.0,
                      height: 250.0,
                      alignment: Alignment.center,
                      child: Image.asset("assets/buck.jpg"),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(20, 10, 1, 20),
                    child: Text(
                      'นายกิตติศักดิ์ สุทธิกิตติพงศ์ 6350110001 ',
                      style: TextStyle(fontSize: 20),
                    ),
                  ),
                  Container(
                    width: 400.0,
                    height: 250.0,
                    alignment: Alignment.center,
                    child: Image.asset("assets/boy.jpg"),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                      'นายชญานนท์ หมวดสง 6350110002 ',
                      style: TextStyle(fontSize: 20),
                    ),
                  ),
                  SizedBox(height: 10),
                  Text('Information and Computer Management',style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),)
                ],
              ),
            )
        )
    );
  }
}
